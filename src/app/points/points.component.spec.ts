/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {NO_ERRORS_SCHEMA, Component} from '@angular/core';
import {PointsComponent} from './points.component';
import {ApiService} from '../services/api.service'
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Observable, AsyncSubject} from 'rxjs';

let apiServiceStub: Partial<ApiService>;

describe('PointsComponent', () => {
    let component: PointsComponent;
    let fixture: ComponentFixture<PointsComponent>;

    beforeEach(async () => {

        // we need this because we call getPoints with apiService.get onInit
        apiServiceStub = {
            get(path: string) {
                return new AsyncSubject();
            }
        };

        TestBed.configureTestingModule({
            declarations: [PointsComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [{provide: ApiService, useValue: apiServiceStub}],
            imports: [HttpClientTestingModule],
        })
            .compileComponents();

        fixture = TestBed.createComponent(PointsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should render <Point adding section> title in a span tag', async(() => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('span').textContent).toContain('Point adding section');
    }));

    it('should render <Square finder sectionn> title in the last span tag', async(() => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        let els = compiled.querySelectorAll('span')
        let last = [].slice.call(els).pop();
        expect(last.textContent).toContain('Square finder section');
    }));
});
