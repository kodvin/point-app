import {Component, OnInit} from '@angular/core';
import {Point} from '../model/point';
import {ApiService} from '../services/api.service';


// Main component that will retrieve points and hold other component
@Component({
    selector: 'app-points',
    templateUrl: './points.component.html',
    styleUrls: ['./points.component.css'],
})
export class PointsComponent implements OnInit {
    newPoint: Point = new Point(0, 0);
    points: Point[];
    messages: string[];
    isValidX = true;
    isValidY = true;

    constructor(private api: ApiService) {
    }

    ngOnInit() {
        this.getPoints();
    }

    addPoint(point: Point) {
        if (!this.isPointValid(point)) return;
        this.api
            .post('/api/point', point)
            .subscribe(
                () => {
                    this.newPoint.x = 0;
                    this.newPoint.y = 0;
                    this.getPoints();
                },
                error => console.error('error --', error)
            );

    }

    removePoint(point: Point) {
        if (!point || !point.hasOwnProperty('x') || !point.hasOwnProperty('y')) {
            return;
        }
        this.api
            .del('/api/point', point)
            .subscribe(
                () => {
                    this.getPoints();
                },
                error => console.error('error --', error)
            );
    }


    getPoints() {
        this.api.get('/api/points')
            .subscribe(
                data => {
                    if (data && data.hasOwnProperty('points') && Array.isArray(data['points'])) {
                        this.points = data['points'];
                    }
                },
                error => console.error('error --', error)
            );
    }

    clearPoints() {
        this.api
            .del('/api/points')
            .subscribe(
                () => this.points = [],
                error => console.error('error --', error)
            );
    }

    importFinished(messages) {
        this.messages = messages;
        this.getPoints();
    }

    clearMessages = () => this.messages = [];

    private isPointValid(point) {
        if (!point) {
            return false;
        }
        this.isValidX = point.hasOwnProperty('x') && this.isInputNumberValid(point.x);
        this.isValidY = point.hasOwnProperty('y') && this.isInputNumberValid(point.y);

        return this.isValidX && this.isValidY;
    }

    private isInputNumberValid(inputText) {
        if (!this.isIntegerNumber(inputText)) return false;
        if (+inputText > 5000 || +inputText < -5000) return false;
        return true;
    }

    private isIntegerNumber(inputText) {
        const n = Math.floor(Number(inputText));
        return n !== Infinity && String(n) === String(inputText);
    }
}
