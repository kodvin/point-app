import {Point} from './point';

export class Square {
    points: Point[];

    constructor(points) {
        this.points = [
            new Point(points[0], points[1]),
            new Point(points[2], points[3]),
            new Point(points[4], points[5]),
            new Point(points[6], points[7])
        ];
    }
}