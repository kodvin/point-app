export interface SortBy {
    by: string;
    direction: number;
}
