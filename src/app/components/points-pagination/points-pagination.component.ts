import {Component, Output, Input, EventEmitter} from '@angular/core';
import {Point} from '../../model/point';
import {FileReaderEvent} from '../../model/file-reader-event';
import {ApiService} from '../../services/api.service';
import {SortBy} from '../../model/sort-by';

@Component({
    selector: 'app-points-pagination',
    templateUrl: './points-pagination.component.html',
    styleUrls: ['./points-pagination.component.css']
})
export class PointsPaginationComponent {

    @Input() points: Point[];
    @Output() removePoint = new EventEmitter<Point>();
    @Output() importFinished = new EventEmitter<string[]>();
    @Output() clearPoints = new EventEmitter<boolean>();
    pageSize = 5;
    page = 1;
    sortBy: SortBy = {by: '', direction: 0};

    constructor(private api: ApiService) {
    }

    ngOnChanges() {
        this.sort();
    }

    resort(by) {
        if(this.sortBy.by === by) {
            if(this.sortBy.direction === 0) {
                this.sortBy.direction = -1;
            } else {
                this.sortBy.direction *= -1;
            }
        } else {
            this.sortBy.by = (this.sortBy.by === 'x') ? 'y' : 'x';
            this.sortBy.direction = -1;
        }
        this.sort();
    }

    sort() {
        let sortX = 'x', sortY = 'y';
        if (!this.points || (this.sortBy.by !== sortX && this.sortBy.by !== sortY)) {
            return;
        }
        if (this.sortBy.direction > 0) {
            this.points
                .sort((a, b) => { 
                    return (a[this.sortBy.by] > b[this.sortBy.by]) ? -1 : ((b[this.sortBy.by] > a[this.sortBy.by]) ? 1 : 0);
                });
        } else if (this.sortBy.direction < 0) {
            this.points
                .sort((a, b) => { 
                    return (a[this.sortBy.by] > b[this.sortBy.by]) ? 1 : ((b[this.sortBy.by] > a[this.sortBy.by]) ? -1 : 0);
                });
        }
    }

    remove(point) {
        this.removePoint.emit(point);
    }
    removePoints() {
        this.clearPoints.emit(true);
    }
    serveFile(data: string) {
        if (!data) {
            return;
        }
        const blob = new Blob([data], {type: 'text/csv'});
        const url = window.URL.createObjectURL(blob);
        window.open(url);
    }

    // get current list of points, formmat and serve this list as csv file
    saveCurrent() {
        if(!this.points) return;
        let formattedString = '';
        const currentPoints = this.points;
        for (let i = 0; i < currentPoints.length; i++) {
            formattedString += `${currentPoints[i].x} ${currentPoints[i].y}\n`;
        }
        this.serveFile(formattedString);
    }

    importFile(file: any) {
        if (!file) {
            return;
        }
        const reader = new FileReader();
        const path = '/api/import_points';
        reader.readAsText(file, 'UTF-8');
        reader.onload = (evt: FileReaderEvent) => {
            this.api
                .post(path, {'text': evt.target.result})
                .subscribe(
                    data => {
                        if (!data['OK']) {
                            this.importFinished.emit(data['errorMessages']);
                        } else {
                            this.importFinished.emit([]);
                        }
                    },
                    error => console.error('error --', error)
                );
        };
    }

}
