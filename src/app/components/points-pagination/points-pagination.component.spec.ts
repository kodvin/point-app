/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {PointsPaginationComponent} from './points-pagination.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {ApiService} from '../../services/api.service';

let apiServiceStub: Partial<ApiService>;

describe('PointsPaginationComponent', () => {
    let component: PointsPaginationComponent;
    let fixture: ComponentFixture<PointsPaginationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PointsPaginationComponent],
            schemas: [NO_ERRORS_SCHEMA],
            imports: [NgxPaginationModule],
            providers: [{provide: ApiService, useValue: apiServiceStub}],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PointsPaginationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
