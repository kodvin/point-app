import { Component, Input } from '@angular/core';
import { Point } from '../../model/point';

@Component({
  selector: 'point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.css']
})
export class PointComponent {
	@Input() point: Point;
}
