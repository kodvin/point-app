import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import {SquareFinderComponent} from './square-finder.component';
import {PointComponent} from '../point/point.component';

describe('SquareFinderComponent', () => {
    let component: SquareFinderComponent;
    let fixture: ComponentFixture<SquareFinderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SquareFinderComponent, PointComponent],
            imports: [NgxPaginationModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SquareFinderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
