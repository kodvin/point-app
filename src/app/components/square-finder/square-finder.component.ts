import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {WebSocketSubject} from 'rxjs/observable/dom/WebSocketSubject';
import {Point} from '../../model/point';
import {Square} from '../../model/square';


@Component({
    selector: 'app-square-finder',
    templateUrl: './square-finder.component.html',
    styleUrls: ['./square-finder.component.css']
})
export class SquareFinderComponent implements OnInit {

    totalSquareCount = 0;
    loadingText = '';
    isCalculating = false;
    squares: Square[] = [];
    pageSize = 5;
    page = 1;
    private socket$: WebSocketSubject<string>;

    constructor() {
        this.socket$ = WebSocketSubject.create('ws://localhost:8999');

        this.socket$
            .subscribe(
                (message) => {
                    if (!message.hasOwnProperty('state') || !message['state']) return;
                    if (message['state'] === 'SQUAREFOUND') {
                        this.squares.push(new Square(message['content']));
                        this.totalSquareCount = this.squares.length;
                    } else if (message['state'] === 'EXITGENERATION') {
                        this.isCalculating = false;
                    } else {
                        this.loadingText = message['content'];
                    }
                },
                (err) => {
                    console.log(err);
                },
                () => console.warn('Completed!')
            );
    }

    ngOnInit() {
    }

    findSquares() {
        if (this.isCalculating) {
            return;
        }
        this.isCalculating = true;
        this.socket$.next('findSquares');
        this.squares = [];

    }

}
