/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {PointStorageComponent} from './point-storage.component';
import {ApiService} from '../../services/api.service';
import {HttpClientModule} from '@angular/common/http';

describe('PointStorageComponent', () => {
    let component: PointStorageComponent;
    let fixture: ComponentFixture<PointStorageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PointStorageComponent],
            schemas: [NO_ERRORS_SCHEMA],
            providers: [ApiService],
            imports: [HttpClientModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PointStorageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
