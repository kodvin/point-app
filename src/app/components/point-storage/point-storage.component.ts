import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
    selector: 'app-point-storage',
    templateUrl: './point-storage.component.html',
    styleUrls: ['./point-storage.component.css']
})
export class PointStorageComponent implements OnInit {
    @Output() listLoaded = new EventEmitter();
    listNames: string[];

    constructor(private api: ApiService) {
    }

    ngOnInit() {
        this.getListNames();
    }

    getListNames() {
        this.api.get('/api/list_names')
            .subscribe(
                data => this.listNames = data['listNames'],
                error => console.error('error --', error)
            );
    }

    saveList(listName) {
        if (!listName) {
            return;
        }
        this.api.post('/api/list', {name: listName})
            .subscribe(() => this.getListNames(), error => console.error('error --', error));
    }

    loadList(listName) {
        if (!listName) {
            return;
        }
        this.api.post('/api/load_list', {name: listName})
            .subscribe(() => {
                this.getListNames();
                this.listLoaded.emit(true);
            }, error => console.error('error --', error));
    }

    deleteList(listName) {
        if (!listName) {
            return;
        }
        this.api.del('/api/list', {name: listName})
            .subscribe(() => {
                this.getListNames();
                this.listLoaded.emit(true);
            }, error => console.error('error --', error));
    }
}

