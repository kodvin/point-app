import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';

import {AppComponent} from './app.component';
import {PointsComponent} from './points/points.component';
import {PointComponent} from './components/point/point.component';
import {ApiService} from './services/api.service';
import {PointsPaginationComponent} from './components/points-pagination/points-pagination.component';
import {PointStorageComponent} from './components/point-storage/point-storage.component';
import {SquareFinderComponent} from './components/square-finder/square-finder.component';

@NgModule({
    declarations: [
        AppComponent,
        PointsComponent,
        PointsPaginationComponent,
        PointStorageComponent,
        SquareFinderComponent,
        PointComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgxPaginationModule
    ],
    providers: [
        ApiService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
