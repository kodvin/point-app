//emulated databaseless storage
module.exports = {
    createConnection: function (host, user, password) {
        let points = [];
        let lists = {};
        return {
            connect() {
                console.log('connected to databaseless database');
            },
            addPoint(point) {
                for (let i = 0; i < points.length; i++) {
                    if (points[i]['x'] === point['x'] && points[i]['y'] === point['y']) return true;
                }
                if (points.length >= 10000) return;

                points.push(point);
            },
            getPoints() {
                return points;
            },
            setPoints(newPoints) {
                points = newPoints;
            },
            exist(point) {
                if (!points) return;
                for (let i = 0; i < points.length; i++) {
                    if (points[i]['x'] === point['x'] && points[i]['y'] === point['y']) return true;
                }
                return false;
            },
            deletePoint(point) {
                for (let i = 0; i < points.length; i++) {
                    console.log('remove point', points[i], point);
                    if (points[i]['x'] === point['x'] && points[i]['y'] === point['y']) {
                        console.log('remove point', points[i], point);
                        points.splice(i, 1);
                    }
                }
            },
            saveList(listName, points) {
                lists[listName] = points;
            },
            getList(listName) {
                return lists[listName];
            },
            deleteList(listName) {
                delete lists[listName];
            },
            getLists() {
                return lists;
            },
            getListNames() {
                return Object.keys(lists);
            }
        }
    }
} 

