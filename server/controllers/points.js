//controller for point handling: add, delete, import...
let Point = require('../models/point');
let validation = require('../bin/validation');

exports.getPoints = (req, res) => {
  res.send({'points' : Point.getPoints()});
};

exports.postPoint = (req, res) => {
	let point = new Point(req.body.x, req.body.y);
	Point.addPoint(point);
  	res.send(Point.getPoints());
};

exports.deletePoint = (req, res) => {
    let point = new Point(req.body.x, req.body.y);
    Point.deletePoint(point);

    res.send(Point.getPoints());
};

exports.deletePoints = (req, res) => {
	Point.setPoints([]);
	res.send(true);
};

exports.importPoints = (req, res) => {  
    let textArray =  req.body.text.split("\n");
    let errorMessages = [];
    for(let i = 0; i < textArray.length;i++) {
    	let result = validation.isValid(textArray[i]);
    	if(result.value === true){
    		let coordinates = textArray[i].split(' ');
    		Point.addPoint({x: +coordinates[0], y: +coordinates[1]})
    	} else {
    		errorMessages.push(result.message);
    	}
    }
    if (errorMessages.length === 0) { 
    	res.send({'OK': true});
	} else {
		res.send({"OK": false, errorMessages: errorMessages});
	}
};
