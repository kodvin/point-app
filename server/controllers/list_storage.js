// Controller for list of points handling
let List = require('../models/list');
let Point = require('../models/point');

exports.saveList = (req, res) => {
    let listName = req.body.name;
    if (!listName) return res.send(false);

    List.saveList(listName, Point.getPoints());

    res.send(true);
};

exports.loadList = (req, res) => {
    let listName = req.body.name;
    if (!listName) return res.send(false);

    let list = List.getList(listName);
    Point.setPoints(list);

    res.send(true);
};

exports.deleteList = (req, res) => {
    let listName = req.body.name;
    if (!listName) return res.send(false);

    List.deleteList(listName);

    res.send(true);
};

exports.getListNames = (req, res) => {
    let names = List.getListNames();

    res.send({listNames: names});
};
