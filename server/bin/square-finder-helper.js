//all the helping functions for square finding
module.exports = {

    // no need for expensive sqrt because every distance will be squared
    getDistanceSquared(x1, y1, x2, y2) {
        return Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2);
    },

    // return center point between two points
    getCenter(x1, y1, x2, y2) {
        let centerX = (x1 + x2) / 2;
        let centerY = (y1 + y2) / 2;

        return [centerX, centerY];
    },

    isDistanceSquaredEqual(pp1, pp2) {
        let d1 = this.getDsitanceSquared(pp1[0], pp1[1], pp1[2], pp1[3]);
        let d2 = this.getDsitanceSquared(pp2[0], pp2[1], pp2[2], pp2[3]);

        return  d1 === d2;
    },

    isCenterEqual(pp1, pp2) {
        let c1 = this.getCenter(pp1[0], pp1[1], pp1[2], pp1[3]);
        let c2 = this.getCenter(pp2[0], pp2[1], pp2[2], pp2[3]);
        return (Math.abs(c1[0] - c2[0]) < Number.EPSILON &&
            Math.abs(c1[1] - c2[1]) < Number.EPSILON);
    },

    isSidesEqual(pp1, pp2) {
        // because we check centers before we know that centers match that is why we get intersecting pairs
        // that form a square abcd
        // pp1(a, c), pp2(b,d), pp1 = [x1, y1, x2, y2]
        //AB==BC==CD==DA, we can check squared
        let ab = this.getDistanceSquared(pp1[0], pp1[1], pp2[0], pp2[1]);
        let bc = this.getDistanceSquared(pp2[0], pp2[1], pp1[2], pp1[3]);
        let cd = this.getDistanceSquared(pp1[2], pp1[3], pp2[2], pp2[3]);
        let da = this.getDistanceSquared(pp2[2], pp2[3], pp1[0], pp1[1]);

        return (ab === bc) && (bc === cd) && (cd === da) && (ab === da) 
    },

    //this will work only for integer x and y
    //first check if centers are equal. remove parallel lines but rectangular shapes can be formed
    //second distance between a pair of points must be equal. 
    isSquare(pp1, pp2) {
        //format [x11,y11,x12,y12][x21,y21,x22,y22]
        //after center check we can get rectangles
        if (!this.isCenterEqual(pp1, pp2)) return false;
        //after side check we get squares
        if (!this.isSidesEqual(pp1, pp2)) return false;

        return true;
    },

    getPossiblePairs(points) {
        if (!points) return [];
        //todo double check if the size is right
        let len = points.length;
        let pointPairs = new Array(len * (len - 1) / 2);
        pointPairs.fill(new Int32Array(0,0,0,0,0));
        let count = 0;
        for (let i = 0; i < len; i++) {
            // point pair p1,p2 is same as p2,p1 that is why we start from i, +1 so that we eliminate p1,p1 pair
            for (let j = i + 1; j < len; j++) {
                pointPairs[count] = (new Int32Array([
                    points[i]['x'], points[i]['y'], points[j]['x'], points[j]['y'],
                    this.getDistanceSquared(points[i]['x'], points[i]['y'], points[j]['x'], points[j]['y'])
                ]));
                count++;
            }
        }

        return pointPairs;
    },

    //get squares on the go, the problem is that it is cpu intensive that and the browser does not update view
    //this will only unblock backend at every yield, but sorting might take a lot of time, so still blocking
    getSquaresNonBlocking(generator, ws) {
        setTimeout(() => {
            let next = generator.next();
            if (!next.done) {
                ws.send(JSON.stringify(next.value));
                this.getSquaresNonBlocking(generator, ws)
            } else {
                ws.send(JSON.stringify(
                    {"state": "EXITGENERATION", "content": "Found all squares in the possible point space"}
                ));
            }
        }, 0);
    }
};