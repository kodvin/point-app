let Point = require('../models/point');
let sfh = require('./square-finder-helper');

//Generator that will return either a message on the status or a square representation
//this will only work for integer x,y points due to float presicion
function* findSquaresGenerator(points) {
    if(!points || points.length < 4) {
        yield {'state': 'FINISHED', "content": "Too few points"};
        return;
    }
    //DS - distance squared
    const X1 = 0, Y1 = 1, X2 = 2, Y2 = 3, DS = 4;
    console.time('squaresearch');
    yield {'state': 'GENERATING', "content": "Generating point pairs"};
    let pointPairs = sfh.getPossiblePairs(points);
    
    yield {'state': 'SORTING', "content": "Sorting point pairs by distance between them"};
    pointPairs.sort((a, b) => {
        return a[DS] - b[DS];
    });
    let start = 0, end = 0,
        len = pointPairs.length - 1;

    yield {'state': 'ITERATING', "content": "Finding squares" };
    for (let i = 0; i < len; i++) {
        //distances between point pairs equalyti indicates that it can be asquare
        if (pointPairs[i][DS] === pointPairs[i + 1][DS]) {
            end = i + 1;
            continue;
        }
        // when current pair(i, i+1 )distances are not equal we check all that were equal before
        for (let i = start; i <= end; i++) {
            for (let j = i + 1; j <= end; j++) {
            // when current pair(i, i+1 )distances are not equal we check all that were equal before
                if (!sfh.isSquare(pointPairs[i], pointPairs[j])) continue;
                yield {"state": "SQUAREFOUND", "content": [
                    pointPairs[i][X1], pointPairs[i][Y1],
                    pointPairs[i][X2], pointPairs[i][Y2],
                    pointPairs[j][X1], pointPairs[j][Y1],
                    pointPairs[j][X2], pointPairs[j][Y2],
                ]};
            }
        }
        start = end = i + 1;
    }
    // check if final pair can be a square
    if (sfh.isSquare(pointPairs[start], pointPairs[end])) {
        yield {"state": "SQUAREFOUND", "content": [
            pointPairs[start][X1], pointPairs[end][Y1],
            pointPairs[start][X2], pointPairs[end][Y2],
            pointPairs[start][X1], pointPairs[end][Y1],
            pointPairs[start][X2], pointPairs[end][Y2],
        ]};        
    }
    console.timeEnd('squaresearch');
    yield {'state': 'FINISHED', "content": "Found all squares in the possiible point space"};
}

//receives websocket for message sending to frontend
module.exports = {
    getGenerator() {
        return findSquaresGenerator(Point.getPoints());
    },
    startGeneration(generator, ws) {
        sfh.getSquaresNonBlocking(generator, ws);
    }
};