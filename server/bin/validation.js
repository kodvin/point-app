let Point = require('../models/point');
let lowerLimit = -5000;
let upperLimit = 5000;

module.exports = {
    isValid(line) {
        try {
            let coordinates = line.split(' ');
            if (!Number.isInteger(+coordinates[0]) || !Number.isInteger(+coordinates[1])) {
                return {
                    value: false,
                    message: `line: "${line}" could not be parsed(x or y are not integers)`
                }
            } else if (Point.exist({x: +coordinates[0], y: +coordinates[1]})) {
                return {
                    value: false,
                    message: `line: "${line}". Point already exists`
                }
            } else if (+coordinates[0] > upperLimit ||
                +coordinates[0] < lowerLimit ||
                +coordinates[1] > upperLimit ||
                +coordinates[1] < lowerLimit) {
                return {
                    value: false,
                    message: `line: "${line}" has a number out of bounds`
                }
            }

            return {
                value: true,
                message: `line: "${line}" is valid`
            }
        } catch (err) {
            console.log(err);
            return {
                value: false,
                message: `line: "${line}" could not be parsed(generic)`
            }
        }
    }
};