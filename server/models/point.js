/** point.js **/
const dbless = require('../db/dbless');
//here we can change to database connection and add logic for db retrieval
let con = dbless.createConnection();

let Point = function (x, y) {
    this.x = x;
    this.y = y;
};
//use static methods for data retrieval and setting
Point.addPoint = con.addPoint;
Point.getPoints = con.getPoints;
Point.setPoints = con.setPoints;
Point.exist = con.exist;
Point.deletePoint = con.deletePoint;

module.exports = Point;