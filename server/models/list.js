/** point.js **/
const dbless = require('../db/dbless');

//here we can change to database connection and add logic for db retrieval
let con = dbless.createConnection();

let List = function () {
};

List.saveList = con.saveList;
List.getList = con.getList;
List.deleteList = con.deleteList;
List.getListNames = con.getListNames;

module.exports = List;