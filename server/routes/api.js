const express = require('express');
const router = express.Router();

const pointsControllers = require('../controllers/points');
const listStorageControllers = require('../controllers/list_storage');


/* GET return all points */
router.get('/points', pointsControllers.getPoints);
/* POST add single point. */
router.post('/point', pointsControllers.postPoint);
/* DElETE add single point. */
router.delete('/point', pointsControllers.deletePoint);
/* POST delete all points. */
router.delete('/points', pointsControllers.deletePoints);
/* POST import list of points from text */
router.post('/import_points', pointsControllers.importPoints);


/* POST save new list(or replace existing) */
router.post('/list', listStorageControllers.saveList);
/* POST copy list from storage to current list. */
router.post('/load_list', listStorageControllers.loadList);
/* DElETE delete list */
router.delete('/list', listStorageControllers.deleteList);
/* POST delete all points. */
router.get('/list_names', listStorageControllers.getListNames);


module.exports = router;