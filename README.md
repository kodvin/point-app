# PointApp

## Environment 
Run `npm install @angular/cli -g`
To install packages run `yarn install`
In working directory `npm link`
For cleanup `npm unlink` and `npm uninstall @angular/cli -g`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Development server
Run `nodemon server.js` for a dev server. Navigate to `http://localhost:3000/`. 


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


When input is larger than 3000 points there is not enough default memory for node
run server with `nodemon server.js --max-old-space-size=20480`
for up to 8000 10240 is enough
for 6000 run time was 96 seconds
for 4000 ~15-20 seconds
