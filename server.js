const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');

// Get our API routes
const api = require('./server/routes/api');
const squareFinder = require('./server/bin/square-finder');

const app = express();

// Parsers for POST data
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false}));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));

//Setup websocket server
const WebSocket = require('ws');
const wss = new WebSocket.Server({port: 8999});

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        if (message === "findSquares") {
            ws.send(JSON.stringify({"state": "FINDSQUARES", "content": "Started finding squares"}));
            squareFinder.startGeneration(
            	squareFinder.getGenerator(),
            	ws
        	);
        }
    });
});



